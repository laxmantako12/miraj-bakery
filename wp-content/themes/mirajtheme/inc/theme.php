<?php

/**
 * Allow KTM Office to access the site even though WP Force Login is active
 */
add_filter( 'v_forcelogin_bypass', function() {
	$allowed_ip_addresses = [
		'158.174.69.114',
	];

	return in_array( $_SERVER['REMOTE_ADDR'], $allowed_ip_addresses );
});

/**
 * Pre get posts filter
 */
add_filter( 'pre_get_posts', function( $query ) {

	if ( $query->is_main_query() && ! is_admin() ) {

		// Redirect empty searches to search page, not home
		if ( isset( $_GET['s'] ) && empty( $_GET['s'] ) ) {
			$query->is_search = true;
			$query->is_home = false;
		}

		/*
			$query->set( 'posts_per_page', 7 );
			$query->set( 'nopaging', true );
			$query->set( 'orderby', 'menu_order' );
			$query->set( 'order', 'ASC' );
			$query->set( 'post_type', 'wptemplate_cpt' );
		*/
	}

	return $query;
});

add_action( 'admin_bar_menu', function( $admin_bar ) {
	if ( ! current_user_can( 'administrator' ) || is_admin() ) {
		return;
	}
	global $template;

	$template_name = str_replace( get_template_directory() . '/', '', $template );
	$args = [
		'id'    => 'my-template',
		'title' => 'Template: ' . $template_name,
		'href'  => '#',
		'meta'  => [
			'title' => $template_name,
		],
	];

	$admin_bar->add_menu( $args );
},  100 );

/**
 * Comment
 * @ params $background_color
 */
function KTM_background_color( $background_color ) {
	$component_bgcolor = '';

	switch ( $background_color ) {
		case 'gray':
			$component_bgcolor = 'bg-color-gray text-color-dark';
			break;
		default:
			$component_bgcolor = 'bg-color-white text-color-dark';
			break;
	}

	return $component_bgcolor;
}

function mz_change_product_price_display( $price ) {
    $price .= '<span class="qnty-format"> per pound</span>';
    return $price;
}
add_filter( 'woocommerce_get_price_html', 'mz_change_product_price_display' );
// add_filter( 'woocommerce_cart_item_price', 'mz_change_product_price_display' );

function wp_echo_qty_front_add_cart() {
 echo '<div class="qty">Pound </div>'; 
}
add_action( 'woocommerce_before_add_to_cart_quantity', 'wp_echo_qty_front_add_cart' );

add_filter('gettext', 'translate_reply');
add_filter('ngettext', 'translate_reply');

function translate_reply($translated) {
$translated = str_ireplace('Quantity', 'Pound', $translated);
return $translated;
}