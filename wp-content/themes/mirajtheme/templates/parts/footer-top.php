<?php
/**
 * The template for displaying footer top section
 *
 * @package WordPress
 */

$footer_title_top  = get_field( 'footer_title_top', 'option' );
$description       = get_field( 'newsletter_description', 'option' );
$facebook_link     = get_field( 'facebook_link', 'option' );
$instagram_link    = get_field( 'instagram_link', 'option' );
$youtube_link      = get_field( 'youtube_link', 'option' );
?>

<div class="subscribe">
	<div class="container">
		<?php if ( $footer_title_top ) : ?>
			<h2><?php echo $footer_title_top; ?> <span class="icon-muffin"></span></h2>
		<?php endif; ?>
		<?php
		if ( $description ) {
			echo '<div class="slogan">' . $description . '</div>';
		}
		?>

		<?php
		if ( $facebook_link || $instagram_link || $youtube_link ) {
			echo '<div class="social-icons"><ul>';
			if ( $facebook_link ) {
				echo '<li><a href="' . esc_url( $facebook_link ) . '">
				<span class="icon-facebook"></span>
				</a></li>';
			}

			if ( $instagram_link ) {
				echo '<li><a href="' . esc_url( $instagram_link ) . '">

				<span class="icon-instagram"></span>
				</a></li>';
			}

			if ( $youtube_link ) {
				echo '<li><a href="' . esc_url( $youtube_link ) . '">

				<span class="icon-youtube"></span></a></li>';
			}
			echo '</ul></div>';
		}
		?>
	</div>
</div>
