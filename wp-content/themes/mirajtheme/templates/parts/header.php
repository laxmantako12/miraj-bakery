<header class="header site-header">
	<?php $logo = get_field( 'logo', 'option' ); ?>
    <!-- nav starts -->
    <nav class="navbar navbar-expand-lg navbar-light bg-theme">
        <div class="container">

		<a class="site-logo navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">

				<span class="icon-muffin"></span><b class="block">Miraz</b> <span class="line">bakery</span>
			</a>

            <button class="navbar-toggler">
                <span class="navbar-icon"></span>
                <span class="navbar-icon"></span>
                <span class="navbar-icon"></span>
                <span class="navbar-icon"></span>
            </button>


            <div class="collapse navbar-collapse">
            	<?php
				wp_nav_menu([
					'theme_location' => 'left-menu',
					'depth' => 1,
					'container' => false,
					'menu_class' => 'navbar-nav mr-auto',
				]);
				?>
				<?php
				wp_nav_menu([
					'theme_location' => 'right-menu',
					'depth' => 1,
					'container' => false,
					'menu_class' => 'navbar-nav ml-auto',
				]);
				?>
				<a class="cart-customlocation" href="<?php echo wc_get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>"> <span><?php echo sprintf ( _n( '%d', '%d', WC()->cart->get_cart_contents_count() ), WC()->cart->get_cart_contents_count() ); ?></span></a>
            </div>
        </div>
    </nav>
    <!-- end top nav  -->
</header>
