<?php
/**
 * The template for displaying footer section
 *
 * @package WordPress
 */

$copyright_text = get_field( 'copyright_text', 'option' );
?>

<div class="footer__btm">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-md-4">
				<div class="copyright">
					<p><?php echo esc_html( $copyright_text ); ?></p>
				</div>
			</div>
			<div class="col-lg-6 col-md-8">
				<div class="footer-menu">
					<div class="designby">
						<p>Developed By - <a target="_blank" href="http://codeconsciousness.com/">codeconsciousness.com</a></p>
					</div>
					<?php
					// wp_nav_menu([
					// 	'theme_location' => 'footer-menu',
					// 	'depth' => 1,
					// 	'container' => false,
					// ]);
					?>
				</div>
			</div>
<!-- 			<div class="col-lg-4">
				<div class="designby">
					<p>Developed By: <a target="_blank" href="http://codeconsciousness.com/">Code Consciousness</a></p>
				</div>
			</div> -->
		</div>
	</div>
</div>
