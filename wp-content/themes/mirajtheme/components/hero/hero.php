<?php
/**
 * The template for displaying hero section
 *
 * @package WordPress
 */

$component_class = [
	'component',
	'component--hero',
	'hero',
];
?>
<section class="<?php echo esc_attr( implode( ' ', $component_class ) ); ?>">
	<div class="fullwidth banner-slider">
		<?php
		if( have_rows('hero_section') ):
			while( have_rows('hero_section') ) : the_row();
				$bg_image = get_sub_field( 'hero_image', get_the_ID() );
				$content  = get_sub_field( 'hero_content', get_the_ID() );
		?>
			<div class="banner-item">
				<?php echo wp_get_attachment_image( $bg_image, '', true, [ 'class' => 'img-responsive' ] ); ?>
				<div class="caption">
					<?php echo wp_kses_post( $content ); ?>
				</div>
			</div>
		<?php
			endwhile;
		endif;
		?>
	</div>
</section>
