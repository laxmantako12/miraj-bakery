class HeroBanner extends Component {
	init() {
		$( '.banner-slider' ).slick({
			infinite: true,
			dots: true,
			arrows: false,
			slidesToShow: 1,
			slidesToScroll: 1,
		});
	}
}

HeroBanner.addComponent( '.component--hero' );
