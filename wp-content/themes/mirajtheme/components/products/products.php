<?php
/**
 * The template for displaying product section
 *
 * @package WordPress
 */

$component_class = [
	'component',
	'component--product',
	'component--our-product',
	'product-section',
];

$products          = get_sub_field( 'products', get_the_ID() );
$background_color  = get_sub_field( 'background_color', get_the_ID() );
$component_class[] = KTM_background_color( $background_color );

$component_setting_title = get_sub_field( 'component_setting_title', get_the_ID() );
?>

<section class="<?php echo esc_attr( implode( ' ', $component_class ) ); ?>">
	<div class="container">
		<?php if ( $component_setting_title ) : ?>
			<h2><span class="line"><?php echo esc_html( $component_setting_title ); ?></span></h2>
		<?php endif; ?>
		<?php
			$args = [
				'post_type'      => 'product',
				'post_status'    => 'publish',
				'post__in'       => $products,
				'order'          => 'DESC',
				'orderby'        => 'post__in',
				'posts_per_page' => -1,
			];

			$query = new WP_Query( $args );
			if ( $query->have_posts() ) :
				echo '<div class="row">';
				while ( $query->have_posts() ) :
					$query->the_post();
					global $post, $product;
		?>
				<div class="col-lg-4 col-md-6">
					<div class="product-wrapper">
						<div class="image">
							<a href="<?php echo esc_url( get_permalink( $product->get_id() ) ); ?>">
								<?php if ( $product->is_on_sale() ) : ?>
									<?php echo apply_filters( 'woocommerce_sale_flash', '<span class="onsale">' . esc_html__( 'Sale!', 'woocommerce' ) . '</span>', $post, $product ); ?>
								<?php endif; ?>
								<?php echo $product->get_image(); ?>
							</a>
						</div>
						<div class="description">
							<h3><a href="<?php echo esc_url( get_permalink( $product->get_id() ) ); ?>"><?php the_title(); ?></a></h3>
							<p class="price"><?php echo $product->get_price_html(); ?></p>
							<?php woocommerce_template_loop_add_to_cart(); ?>
						</div>
					</div>
				</div>
		<?php
				endwhile;
				echo '</div>';
				wp_reset_postdata();
			endif;
		?>
	</div>
</section>
