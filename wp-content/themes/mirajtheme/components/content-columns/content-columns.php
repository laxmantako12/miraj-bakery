<?php
/**
 * The template for displaying content column section
 *
 * @package WordPress
 */

$component_class = [
	'component',
	'component--content-column',
	'content-column',
	'content-column-section',
];

$component_setting_title = get_sub_field( 'component_setting_title', get_the_ID() );
$content                 = get_sub_field( 'content', get_the_ID() );
$width                   = get_sub_field( 'width', get_the_ID() );
$container_class         = ( 'full-width' == $width  ) ? 'fullwidth' : 'container';

if ( is_array( $content ) || is_object( $content ) ) :
	$row_count = count( $content );
$count     = 1;
foreach ( $content as $contents ) :
	$content_s[] = $contents['column_size'];
	if ( '6' === $contents['column_size'] ) :
		if ( 1 === $count ) :
			$component_class[] = 'column-two';
		endif;
	endif;

	if ( 4 <= $row_count && 2 < $contents['column_size'] ) :
		if ( 1 === $count ) :
			$component_class[] = 'column-spacing';
		endif;
	endif;
	$count++;
endforeach;
endif;

$background_color  = get_sub_field( 'background_color', get_the_ID() );
$component_class[] = KTM_background_color( $background_color );
?>

<section class="<?php echo esc_attr( implode( ' ', $component_class ) ); ?>">
	<div class="<?php echo $container_class; ?>">
		<?php if ( $component_setting_title ) : ?>
			<h2><span class="line"><?php echo $component_setting_title ; ?><span class="icon-muffin"></span></span></h2>
		<?php endif; ?>
		<div class="fullwidth por">
			<?php
			$count     = 1;
			$total_div = 0;
			$i         = 0;
			if ( have_rows( 'content' ) ) {
				while ( have_rows( 'content' ) ) :
					the_row();
					$column_size     = get_sub_field( 'column_size' );
					$center_vertical = get_sub_field( 'center_vertical' );

					if ( '12' === $column_size ) :
						$column_count = 'col-md-12';
					elseif ( '8' === $column_size ) :
						$column_count = 'col-md-8 col-sm-6';
					elseif ( '6' === $column_size ) :
						$column_count = 'col-md-6 col-sm-6';
					elseif ( '4' === $column_size ) :
						$column_count = 'col-md-4 col-sm-6';
					else :
						$column_count = 'col-md-3 col-sm-6';
					endif;

					if ( $center_vertical ) :
						$vertical_align = ' align-items-center d-flex';
					else :
						$vertical_align = '';
					endif;

					if ( 'content' === get_row_layout() ) :
						$content_type = 'content-block';
					else :
						$content_type = 'image-block';
					endif;

					if ( 1 === $count ) :
						echo '<div class="row row-repeater d-flex">';
					endif;

					echo '<div class="col-12 ' . esc_html( $column_count ) . ' ' . esc_html( $vertical_align ) . ' ' . esc_html( $content_type ) . '">';

					if ( 'content' === get_row_layout() ) : ?>
						<div class="fullwidth">
							<div class="content-col">
								<?php echo wp_kses_post( the_sub_field( 'content' ) ); ?>
							</div>
						</div>
					<?php
					else :
					$image_id = get_sub_field( 'image' );
					?>
					<figure class="image-wrapper">
						<?php echo wp_get_attachment_image( $image_id, '', false, [ 'class' => 'img-responsive' ] ); ?>
					</figure>
					<?php
					endif;
					echo '</div>';

					$next_val  = ( isset( $content_s[ $i + 1 ] ) ) ? $content_s[ $i + 1 ] : '0';
					$total_div = $total_div + $column_size;
					$next_div  = $total_div + $next_val;
					if ( $next_div > 12 ) :
						echo '</div>';
						$count     = 0;
						$total_div = 0;
					endif;

					$i++;
					$count++;
				endwhile;
				echo '</div>';
			}
			?>
		</div>
	</div>
</section>
