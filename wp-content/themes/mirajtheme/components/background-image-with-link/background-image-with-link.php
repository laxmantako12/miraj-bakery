<?php
/**
 * The template for displaying background image with link section
 *
 * @package WordPress
 */

$component_class = [
	'component',
	'component--image',
	'component--image-parallax',
	'parallax-section',
];

$background_title = get_sub_field( 'background_title', get_the_ID() );
$background_image = get_sub_field( 'background_image', get_the_ID() );
$background_link  = get_sub_field( 'background_link', get_the_ID() );

$background = '';
if ( $background_image ) {
	$background = 'style="background: url('. $background_image .') no-repeat center;"';
}
?>

<section class="<?php echo esc_attr( implode( ' ', $component_class ) ); ?>" <?php echo $background; ?>>
	<div class="line-animation">
		<div class="line line--top"></div>
		<div class="line line--right"></div>
		<div class="line line--bottom"></div>
		<div class="line line--left"></div>
		<div class="container">
			<div class="parallax-content">
				<?php
				if ( $background_title ) {
					echo '<h2>' . $background_title . '</h2>';
				}
				?>
				<div class="vertical-line"></div>
				<?php
				if ( $background_link ) {
					echo '<a href="' . $background_link['url'] . '" class="btn btn--solid" target="' . $background_link['target'] . '">' . $background_link['title'] . '</a>';
				}
				?>
			</div>
		</div>
	</div>
</section>
