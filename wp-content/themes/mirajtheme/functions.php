<?php

// Dependencies
require_once 'inc/class-ktm-post-type.php';
require_once 'inc/class-ktm-taxonomy.php';
require_once 'inc/class-ktm-ajax.php';

require_once 'inc/ktm-helpers.php';
require_once 'inc/theme.php';
require_once 'inc/acf.php';
require_once 'inc/tinymce.php';
require_once 'inc/ajax-functions.php';
require_once 'inc/sub-nav.php';
require_once 'inc/rest.php';

if ( class_exists( 'GFForms' ) ) {
	require_once 'inc/gravity-forms.php';
}

require_once 'inc/cpt.php';
// require_once 'inc/acf-icons.php';

add_action( 'after_setup_theme', function() {
	register_nav_menu( 'left-menu', __( 'Left Menu', 'wptemplate' ) );
	register_nav_menu( 'right-menu', __( 'Right Menu', 'wptemplate' ) );
	register_nav_menu( 'footer-menu', __( 'Footer Menu', 'wptemplate' ) );

	//add_image_size( 'theme_img_name', width, height, crop(true|false) );
});

add_action( 'wp_enqueue_scripts', function() {
	if ( ! is_admin() ) {
		wp_deregister_script( 'jquery' );
		wp_register_script( 'jquery', get_stylesheet_directory_uri() . '/assets/dist/jquery.min.js', '', '2.2.4', true );

		wp_register_script( 'app', get_stylesheet_directory_uri() . '/assets/dist/app.min.js', [ 'jquery' ], '1.0', true );
		wp_enqueue_script( 'app' );

		wp_register_script( 'custom', get_stylesheet_directory_uri() . '/assets/js/vendor/custom.js', [ 'jquery' ], '1.0', true );
		wp_enqueue_script( 'custom' );

		wp_deregister_script( 'wp-embed' );

		wp_deregister_script( 'gform_placeholder' );

		wp_localize_script(
			'app',
			'theme',
			[
				'ajaxurl' => KTM_get_ajaxurl(),
				'cookieMessageText' => get_field( 'cookie_message_text', 'options', false ),
				'cookieAcceptText' => get_field( 'cookie_accept_text', 'options' ),
			]
		);

		wp_register_style( 'style', get_stylesheet_directory_uri() . '/assets/dist/style.min.css', [], '1.0' );
		wp_enqueue_style( 'style' );

		wp_enqueue_style( 'datepicker-style', 'https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css' );
		wp_enqueue_script( 'js-datepicker', 'https://code.jquery.com/ui/1.12.1/jquery-ui.js', array('jquery'), null, true); 
		
		wp_enqueue_style( 'timepicker-style', '//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css'); 
		wp_enqueue_script( 'js-timepicker', '//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js', array('jquery'), null, true); 
	}
});


/**
 * Show cart contents / total Ajax
 */
add_filter( 'woocommerce_add_to_cart_fragments', 'KTM_woocommerce_header_add_to_cart_fragment' );

function KTM_woocommerce_header_add_to_cart_fragment( $fragments ) {
	global $woocommerce;

	ob_start();

	?>
	<a class="cart-customlocation" href="<?php echo esc_url(wc_get_cart_url()); ?>" title="<?php _e('View your shopping cart', 'woothemes'); ?>"><span><?php echo sprintf(_n('%d', '%d', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count);?></span></a>
	<?php
	$fragments['a.cart-customlocation'] = ob_get_clean();
	return $fragments;
}

/**
 * @snippet       Alter Cart Counter @ WooCommerce Cart Widget
 * @how-to        Get CustomizeWoo.com FREE
 * @author        Rodolfo Melogli
 * @testedwith    WooCommerce 4.6
 * @donate $9     https://businessbloomer.com/bloomer-armada/
 */
 
add_filter( 'woocommerce_cart_contents_count', 'KTM_woocommerce_alter_cart_contents_count', 9999, 1 );
 
function KTM_woocommerce_alter_cart_contents_count( $count ) {
   $count = count( WC()->cart->get_cart() );
   return $count;
}

/**
 * Change number of related products output
 */ 
function woo_related_products_limit() {
	global $product;
	
	$args['posts_per_page'] = 6;
	return $args;
}

add_filter( 'woocommerce_output_related_products_args', 'KTM_related_products_args', 20 );
function KTM_related_products_args( $args ) {
	$args['posts_per_page'] = 3; // 4 related products
	$args['columns'] = 1; // arranged in 2 columns
	return $args;
}

//Remove WooCommerce Tabs - this code removes all 3 tabs - to be more specific just remove actual unset lines 
add_filter( 'woocommerce_product_tabs', 'KTM_woo_remove_product_tabs', 98 );
function KTM_woo_remove_product_tabs( $tabs ) {

    unset( $tabs['description'] );      	// Remove the description tab
    unset( $tabs['reviews'] ); 			// Remove the reviews tab
    unset( $tabs['additional_information'] );  	// Remove the additional information tab

    return $tabs;
}

/**
 * @snippet       Remove Sidebar @ Single Product Page
 * @how-to        Get CustomizeWoo.com FREE
 * @sourcecode    https://businessbloomer.com/?p=19572
 * @author        Rodolfo Melogli
 * @testedwith    WooCommerce 3.2.6
 */
add_action( 'wp', 'KTM_remove_sidebar_product_pages' );
function KTM_remove_sidebar_product_pages() {
	if ( is_product() ) {
		remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );
	}
}

// Split product quantities into multiple cart items.
function KTM_force_individual_cart_items( $cart_item_data, $product_id ) {
	$unique_cart_item_key = md5( microtime() . rand() );
	$cart_item_data['unique_key'] = $unique_cart_item_key;

	return $cart_item_data;
}
add_filter( 'woocommerce_add_cart_item_data', 'KTM_force_individual_cart_items', 10, 2 );

/**
 * Display the custom text field
 * @since 1.0.0
 */
function KTM_create_custom_field() {
	$args = array(
		'id'            => 'minimum_cake_pound',
		'label'         => __( 'Minimum pound', 'cfwc' ),
		'class'			=> 'cfwc-custom-field',
		'desc_tip'      => true,
		'description'   => __( 'Enter minimum pound for cake.', 'ctwc' ),
	);
	woocommerce_wp_text_input( $args );
}
add_action( 'woocommerce_product_options_general_product_data', 'KTM_create_custom_field' );

/**
 * Save the custom field
 * @since 1.0.0
 */
function KTM_save_custom_field( $post_id ) {
	$product = wc_get_product( $post_id );
	$title = isset( $_POST['minimum_cake_pound'] ) ? $_POST['minimum_cake_pound'] : '';
	$product->update_meta_data( 'minimum_cake_pound', sanitize_text_field( $title ) );
	$product->save();
}
add_action( 'woocommerce_process_product_meta', 'KTM_save_custom_field' );

/**
 * Minimum quantity @single @cart pages
 * @since 1.0.0
 */
function KTM_woocommerce_quantity_changes( $args, $product ) {
   
  $minimum = $product->get_meta( 'minimum_cake_pound' );
   if ( ! is_cart() ) {
  	 if( !empty($minimum) ):
  	 	$args['input_value'] = $minimum;
  	 	$args['min_value'] = $minimum;
  	 endif;
   } else {
   	if( !empty($minimum) ):
   		if( $args['input_value'] < $minimum ):
   			$args['input_value'] = $minimum;
   		endif;
   		$args['min_value'] = $minimum;
   	endif; 
   }
   return $args;
}
add_filter( 'woocommerce_quantity_input_args', 'KTM_woocommerce_quantity_changes', 10, 2 );

// Set product quantity added to cart (handling ajax add to cart)
add_filter( 'woocommerce_add_to_cart_quantity','KTM_woocommerce_add_to_cart_quantity_callback', 15, 2 );
function KTM_woocommerce_add_to_cart_quantity_callback( $quantity, $product_id ) {
	$minquantity = get_post_meta( $product_id, 'minimum_cake_pound', true );

    if( $minquantity ) {
        $quantity = $minquantity;
    }
    return $quantity;
}

// before add to cart text
add_filter( 'woocommerce_loop_add_to_cart_link', 'KTM_minimum_qty_before_after_btn', 10, 3 );
function KTM_minimum_qty_before_after_btn( $add_to_cart_html, $product, $args ){
	$product_ids = $args['attributes']['data-product_id'];

	$before = '<div class="cus-wrapper">';
	$minqy = '';
	$after = '</div>';
	if ( $product_ids ) {
		$minquantity = get_post_meta( $product_ids, 'minimum_cake_pound', true );

		if ( $minquantity ) {
			$minqy = '<span class="min-qty-txt">' . $minquantity . 'lb min. req.</span>';
			$after = '';
		}
	}
 
	return $before . $minqy . $add_to_cart_html . $after;
}

add_filter('woocommerce_cart_item_price','KTM_add_user_custom_option_from_session_into_cart',1,3);
function KTM_add_user_custom_option_from_session_into_cart($product_name, $values, $cart_item_key )
{
    $product_ids = $values['product_id'];
    $minquantity = get_post_meta( $product_ids, 'minimum_cake_pound', true );
    $product_name .= '<span class="qnty-format"> per pound</span>';

	if ( $product_ids ) {
		$minquantity = get_post_meta( $product_ids, 'minimum_cake_pound', true );

		if ( $minquantity ) {
			$product_name .= '<div class="min-qty-txt">' . $minquantity . 'lb min. req.</div>';
		}
	}
 
	return $product_name;
}


/**
 * Add the field to the checkout
 */
add_filter( 'woocommerce_checkout_fields' , 'KTM_custom_override_checkout_fields' );
function KTM_custom_override_checkout_fields( $fields ) {
	$fields['order']['order_comments'] = array(
		'label' => __('Cake wishes text', 'woocommerce'),
		'placeholder' => _x('Write wishes on cake', 'placeholder', 'woocommerce')
	);

    $fields['order']['delivery_date'] = array(
    'label'     => __('Delivery date', 'woocommerce'),
    'placeholder'   => _x('Delivery date', 'placeholder', 'woocommerce'),
    'required'  => true,
    'class'     => array('form-row-first'),
    'clear'     => true
     );

     $fields['order']['delivery_time'] = array(
     'label'     => __('Delivery time', 'woocommerce'),
     'placeholder'   => _x('Delivery time', 'placeholder', 'woocommerce'),
     'required'  => true,
     'class'     => array('form-row-last'),
     'clear'     => true
      );

     return $fields;
}

/**
 * Update the order meta with field value
 */
add_action( 'woocommerce_checkout_update_order_meta', 'KTM_checkout_field_update_order_meta' );

function KTM_checkout_field_update_order_meta( $order_id ) {
    if ( ! empty( $_POST['delivery_date'] ) ) {
        update_post_meta( $order_id, '_delivery_date', sanitize_text_field( $_POST['delivery_date'] ) );
    }
    if ( ! empty( $_POST['delivery_time'] ) ) {
        update_post_meta( $order_id, '_delivery_time', sanitize_text_field( $_POST['delivery_time'] ) );
    }
}

/**
 * Display field value on the order edit page
 */
 
add_action( 'woocommerce_admin_order_data_after_shipping_address', 'KTM_checkout_field_display_admin_order_meta', 10, 1 );

function KTM_checkout_field_display_admin_order_meta($order){
    echo '<p><strong>'.__('Delivery date').':</strong> ' . get_post_meta( $order->get_id(), '_delivery_date', true ) . '</p>';
    echo '<p><strong>'.__('Delivery time').':</strong> ' . get_post_meta( $order->get_id(), '_delivery_time', true ) . '</p>';
}

add_action( 'woocommerce_before_shop_loop', 'KTM_woocommerce_output_all_noticess', 10 );
function KTM_woocommerce_output_all_noticess(){
	$currentSegment = '';
	if( is_product_category() ) :
		$urlArray = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
		$segments = explode('/', $urlArray);
		$numSegments = count($segments); 
		$currentSegment = $segments[$numSegments - 2];
	endif;
	$prod_cat_args = array(
	  'taxonomy'     => 'product_cat',
	  'orderby'      => 'name',
	  'empty'        => 0
	);

	$woo_categories = get_categories( $prod_cat_args );
	$return .= '<select id="cake_cat_filter">';
	$return .= '<option value="'.get_permalink( woocommerce_get_page_id( 'shop' ) ).'">Sort by category</option>';
	foreach ( $woo_categories as $woo_cat ) {
	    $woo_cat_id = $woo_cat->term_id;
	    $woo_cat_name = $woo_cat->name;
	    $woo_cat_slug = $woo_cat->slug;
	    $selected = ( $currentSegment == $woo_cat_slug ) ? 'selected' : '';
	    $return .= '<option value="' . get_term_link( $woo_cat_slug, 'product_cat' ) . '" '.$selected.'>' . $woo_cat_name . ' ('.$woo_cat->count.')</option>';
	}
	$return .= '<select id="cake_cat_filter">';
	echo $return;

}


