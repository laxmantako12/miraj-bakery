<footer class="site-footer footer fullwidth">
	<?php
	// if ( empty( get_field('hide_footer_contact' ) ) ) {
	// 	get_template_part( 'templates/parts/footer', 'top' );
	// }
	get_template_part( 'templates/parts/footer', 'top' );
	get_template_part( 'templates/parts/footer', 'bottom' );
	?>
</footer>
<?php wp_footer(); ?>
<script>
jQuery(document).ready(function ($) {
var $ = jQuery
$('body').on('click', '.navbar-toggler', function () {
		$(this).toggleClass('on');
		$('.navbar-collapse').toggleClass('shown');
		$('body').toggleClass('body-overflow-hidden');
		// $('.navbar-collapse').slideToggle();
	});

	function prependMenu() {
    if (jQuery(window).width() < 991) {
        jQuery('.cart-customlocation').prependTo('.navbar');
    } else {
        // jQuery('.cart-customlocation').appendTo('.navbar .ml-auto')
    }
}

//
jQuery(window).on('load resize', function (e) {
    prependMenu();
});

//datepicker checkout page
$( "#delivery_date" ).datepicker();
//timepicker checkout page
$('#delivery_time').timepicker({
	timeFormat: 'h:mm p',
	interval: 60,
	dynamic: false,
	dropdown: true,
	scrollbar: true
});
//shop page category filter
$("#cake_cat_filter").on('change', function(){
	var url = $(this).val(); 
	if (url != '') { 
	    window.location = url;
	}
	return false;
})
});
</script>
</body>
</html>
